package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Order;
import com.example.repository.OrderDao;

@Service
public class OrderService {

	private OrderDao orderDao;
	
	public OrderService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public OrderService(OrderDao orderDao) {
		super();
		this.orderDao = orderDao;
	}
	
	public List<Order> getAllOrders(){
		return orderDao.findAll();
	}
	
	public List<Order> get20Orders(){
		List<Order> allOrders = getAllOrders();
		List<Order> randomOrder = new ArrayList<>();
		Random rand = new Random();
		Random rand2 = new Random();
		Random rand3 = new Random();
		int randomNum = rand3.nextInt(40);
		for(int i = 0; i<randomNum; i++) {
			Order tempOrder = allOrders.get(rand.nextInt(allOrders.size()));
			tempOrder.setOrderCode(rand2.nextInt(400000 - 100000) + 100000);
			randomOrder.add(tempOrder);
		}
		
		return randomOrder;
	}
	
	public void insertOrder(Order o) {
		orderDao.save(o);
	}
	
}
