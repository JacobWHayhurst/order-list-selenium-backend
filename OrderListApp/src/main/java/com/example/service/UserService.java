package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.User;
import com.example.repository.UserDao;

@Service
public class UserService {
	
	private UserDao userDao;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public UserService(UserDao userDao) {
		super();
		this.userDao = userDao;
	}
	
	public void insert(User u) {
		userDao.save(u);
	}
	
	public User getUser(String userName) {
		User user = userDao.findByUsername(userName);
		if(user == null) {
			return null;
		}
		return user;
	}
	
	public boolean verifyUser(String userName, String password) {
		
		User user = getUser(userName);
		
		if (user == null) {
			return false;
		}
		
		if(user.getPassword().equals(password)) {
			return true;
		}
		
		return false;
	}

}
