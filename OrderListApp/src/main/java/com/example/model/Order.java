package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="OrderTable")
public class Order {
	
	@Id
	@Column(name="order_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int orderid;
	
	@Column(name="order_name")
	private int orderCode;
	
	@Column(name="order_item")
	private String orderItem;
	
	public Order() {
		// TODO Auto-generated constructor stub
	}

	public Order(int orderid, int orderCode, String orderItem) {
		super();
		this.orderid = orderid;
		this.orderCode = orderCode;
		this.orderItem = orderItem;
	}
	
	public Order(int orderCode, String orderItem) {
		super();
		this.orderCode = orderCode;
		this.orderItem = orderItem;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public int getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(int orderCode) {
		this.orderCode = orderCode;
	}

	public String getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(String orderItem) {
		this.orderItem = orderItem;
	}

	@Override
	public String toString() {
		return "Order [orderid=" + orderid + ", orderCode=" + orderCode + ", orderItem=" + orderItem + "]";
	}

}
