package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.User;
import com.example.service.UserService;

@RestController
@RequestMapping(value="/users")
@CrossOrigin(origins="*")
public class UserController {
	
	private UserService userServ;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public UserController(UserService userServ) {
		super();
		this.userServ = userServ;
	}
	
	@PostMapping("/all")
	public ResponseEntity<String> insertAllUsers(){
		List<User> uList = new ArrayList<>();
		uList.add(new User("Admin","Admin"));
		uList.add(new User("Admin2","Admin"));
		uList.add(new User("Admin3","Admin"));
		
		for(User u: uList) {
			userServ.insert(u);
		}
		return new ResponseEntity<>("Resource Created", HttpStatus.CREATED);
	}
	
	@PostMapping("/login")
	public ResponseEntity<User> login(@RequestBody User user){
		System.out.println(user.getUsername() + ", " + user.getPassword());
		if(userServ.verifyUser(user.getUsername(), user.getPassword())) {
			return new ResponseEntity<>(userServ.getUser(user.getUsername()), HttpStatus.OK);
		}
		return  new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

}
