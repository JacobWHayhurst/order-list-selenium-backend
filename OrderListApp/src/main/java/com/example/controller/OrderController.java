package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Order;
import com.example.service.OrderService;

@RestController
@RequestMapping(value="/orders")
@CrossOrigin(origins="*")
public class OrderController {
	
	private OrderService orderServ;	
	
	public OrderController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public OrderController(OrderService orderServ) {
		super();
		this.orderServ = orderServ;
	}
	
	@PostMapping("/all")
	public ResponseEntity<String> insertAllUsers(){
		List<Order> oList = new ArrayList<>();
		oList.add(new Order(0,"Tv"));
		oList.add(new Order(0,"PS5"));
		oList.add(new Order(0,"Desk"));
		oList.add(new Order(0,"Mic"));
		oList.add(new Order(0,"Wine"));
		oList.add(new Order(0,"Headphones"));
		oList.add(new Order(0,"Laptop"));
		oList.add(new Order(0,"Nightstand"));
		oList.add(new Order(0,"Lamp"));
		oList.add(new Order(0,"Soda"));
		oList.add(new Order(0,"Papasan"));
		oList.add(new Order(0,"Ryzen 7"));
		oList.add(new Order(0,"Couch"));
		oList.add(new Order(0,"Frozen Pizza"));
		oList.add(new Order(0,"Water Bottle"));
		oList.add(new Order(0,"Gin"));
		oList.add(new Order(0,"Drill"));
		oList.add(new Order(0,"Beer"));
		oList.add(new Order(0,"Blanket"));
		oList.add(new Order(0,"Office Chair"));
		oList.add(new Order(0,"Guitar"));
		oList.add(new Order(0,"Cookies"));
		oList.add(new Order(0,"MTG"));
		oList.add(new Order(0,"Pants"));
		oList.add(new Order(0,"Sweaters"));
		oList.add(new Order(0,"Candy"));
		oList.add(new Order(0,"Webcam"));
		oList.add(new Order(0,"Keyboard"));
		oList.add(new Order(0,"Smart Phone"));
		oList.add(new Order(0,"DBZ Live Action Movie"));
		oList.add(new Order(0,"Red Peppers"));
		oList.add(new Order(0,"Bell Peppers"));
		oList.add(new Order(0,"Tofu"));
		oList.add(new Order(0,"Chips"));
		oList.add(new Order(0,"Paste"));
		oList.add(new Order(0,"3080"));
		oList.add(new Order(0,"Book"));
		oList.add(new Order(0,"Hammer"));
		oList.add(new Order(0,"Nails"));
		oList.add(new Order(0,"Curtains"));
		oList.add(new Order(0,"Computer Case"));
		
		for(Order o: oList) {
			orderServ.insertOrder(o);
		}
		return new ResponseEntity<>("Resource Created", HttpStatus.CREATED);

	}
	
	@GetMapping(value="/all")
	public ResponseEntity<List<Order>> getAllOrders(){
		return new ResponseEntity<>(orderServ.getAllOrders(), HttpStatus.OK);
	}
	
	@GetMapping(value="/rand")
	public ResponseEntity<List<Order>> getRandOrders(){
		return new ResponseEntity<>(orderServ.get20Orders(), HttpStatus.OK);
	}
	
}
